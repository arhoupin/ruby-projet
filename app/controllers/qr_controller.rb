class QrController < ApplicationController
  def index
    @reservation = Reservation.select('reservations.id, reservations.statut, reservations.class_seat, reservations.nombre_passage, reservations.identifiantbillet,datetime(flights.departure_date) as departure_date, flights.departure_airport, flights.arrival_airport, flights.number, flights.duration')
                               .joins("INNER JOIN flights ON flights.id = reservations.flight_id")
                               .where(identifiantbillet: params[:id] ).take
  end
end