class ReservationMailer < ApplicationMailer
  default from: 'no_reply@AirCrash.com'

  def welcome_email
    @user = params[:user]
    @billet = params[:billet]
    @flight = params[:flight]
    @reservation = params[:reservation]
    #@url  = 'http://0.0.0.0:3000/reservations/confirmations/' + @billet
    mail(to: @user.email, subject: 'récapitulatif de la réservation')
  end
end
