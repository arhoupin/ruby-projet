class Reservation < ApplicationRecord
  has_one :flight
  has_one :user
end
