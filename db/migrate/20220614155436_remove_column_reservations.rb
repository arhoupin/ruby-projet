class RemoveColumnReservations < ActiveRecord::Migration[7.0]
  def change
    remove_column :reservations, :id_flight
    remove_column :reservations, :id_user
  end
end
