class RemoveColumnReservationsFlight < ActiveRecord::Migration[7.0]
  def change
    remove_column :reservations, :flights_id
  end
end
